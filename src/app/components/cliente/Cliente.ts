export class Cliente {
    _id: string;
    quadra: string; 
    numero: number; 
    tipo : string; 
    nome: string;
    CPF: number;
    CEP: number;
    endereço: string;
    bairro: string;
    cidade: string;
    estado : string;
    contato: number;
}